$().ready(function() {
	$.datepicker._defaults.dateFormat = 'yy-mm-dd';
	var sel = $('.datatable.ajax'),
		default_options = {
			"sPaginationType": "bootstrap",
			"iDisplayLength": 10,
			"oLanguage": {
		        "sLengthMenu": "<span class='lengthMenu'> _MENU_</span><span class='lengthLabel'>Entries per page:</span>",	
		    },
			"sDom": "<'row-fluid'<'span6'Tl><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"oTableTools": {
				"sSwfPath": "/static/js/datatables-extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
				"aButtons": [
					"copy",
					"print",
					{
						"sExtends":    "collection",
						"sButtonText": 'Save <span class="caret" />',
						"aButtons":    [ "csv" ]
					}
				]
			},
			"oLanguage": {
	            "sSearch": "Search all columns:"
	        },
	        "bStateSave": true,
		    "fnDrawCallback": function( oSettings ) {
		    	$('[data-toggle="tooltip"]', this).tooltip();
		    },
	        "fnStateSave": function(oSettings, oData) {
	        	localStorage.setItem('DataTables_' + this.fnSettings().sTableId, JSON.stringify(oData));
	        },
	        "fnStateLoad": function (oSettings) {
	        	return JSON.parse(localStorage.getItem('DataTables_' + this.fnSettings().sTableId));
	        },
	        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
				oSettings.jqXHR = $.ajax({
						"dataType": 'json',
						"type": "GET",
						"url": sSource,
						"data": aoData
					})
					.done(function(data, textStatus, xhr) {
						if( data.result == 'error' ) {
							fnCallback({result: 'error', iTotalRecords: 0, iTotalDisplayRecords: 0, aaData: []}, textStatus, xhr);
						} else {
							fnCallback(data, textStatus, xhr);
						}
					})
					.fail(function(xhr, textStatus, errorThrown) {
						// Handle error with global ajax error handler
						fnCallback({result: 'error', iTotalRecords: 0, iTotalDisplayRecords: 0, aaData: []}, textStatus, xhr);
					});
			},
		};

	sel.each(function(i) {
		var $this = $(this),
			options = default_options,
			columnOptions = [],
			$thead_th = $('thead th', $this),
			$tfoot_th = $('tfoot th', $this),
			datatable;
		
		options['aoColumns'] = []
		
		if( $this.hasClass('ajax') ) {
			options['bProcessing'] = true;
			options['bServerSide'] = true;
			options['sAjaxSource'] = $this.data('source-url') + '?json'
		}
		
		if( $this.data('create-url') != undefined && $this.data('create-url').length != 0 ) {
			options['oTableTools']['aButtons'].unshift({
		        'sExtends':    'gotoURL',
		        'sButtonText': 'Create',
		        'sGoToURL':    $this.data('create-url'),
			});
		}

		$thead_th.each(function(i) {
			var $th = $(this),
				def = {
					'bSortable': false,
					'bSearchable': false
				},
				columnOption = null;
			if( $th.hasClass('sorting') ) {
				def['bSortable'] = true;
			}
			if( $th.hasClass('filtering') ) {
				def['bSearchable'] = true;
				def['sName'] = $th.data('filter-selector');
				columnOption = {
					'type': $th.data('filter-type'),
				};
				if( columnOption['type'] == 'select' ) {
					columnOption['values'] = $th.data('filter-choices');
				}
			}
			options['aoColumns'][i] = def;
			columnOptions[i] = columnOption;
		});
		
		$this.dataTable(options)
			.columnFilter({
				aoColumns: columnOptions,
				sRangeFormat: '{from} to {to}'
			});
		
	});
	
});
