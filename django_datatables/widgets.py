from django.utils.html import format_html
from django.utils.encoding import force_text, smart_text
from django.forms.util import flatatt
from django.utils import formats, six, datetime_safe
from django.utils.text import slugify
from django.forms.widgets import MediaDefiningClass
from string import Template
from decimal import Decimal
from django.utils.safestring import mark_safe
from django.db import models
from django_datatables.writers.html import HtmlWriter
from django.db.models.base import Model
from django.conf import settings
from django.template import Context, Template
from django.utils.timesince import timesince

class Widget(six.with_metaclass(MediaDefiningClass)):
    css = 'widget'
    is_localized = False
    
    def __init__(self, field, attrs=None):
        self.field = field
        if attrs is not None:
            self.attrs = attrs.copy()
        else:
            self.attrs = {}

    def render_title(self, writer, field, attrs=None):
        """
        Returns field's title rendered using Widget.
        """
        raise NotImplementedError

    def render(self, writer, name, value, data, attrs=None):
        """
        Returns this Widget rendered as HTML, as a Unicode string.
        """
        raise NotImplementedError

    def build_attrs(self, data={}, base_css='widget', extra_css=None, **kwargs):
        "Helper function for building an attribute dictionary."
        attrs = dict(self.attrs, **kwargs)
        attrs['class'] = base_css
        if 'data_tip' in attrs:
            attrs['data-toggle'] = 'tooltip'
            attrs['data-title'] = attrs['data_tip']
            del(attrs['data_tip'])
            if '{' in attrs['data-title']:
                t = Template(attrs['data-title'])
                c = Context({'object': data})
                attrs['data-title'] = t.render(c)
        if 'title_tip' in attrs:
            del(attrs['title_tip'])
        if extra_css:
            for css in extra_css:
                attrs['class'] += u' %s-%s' % (base_css, slugify(u'%s' % css))
        return attrs

    def build_title_attrs(self, base_css='widget', extra_css=None, **kwargs):
        "Helper function for building an attribute dictionary."
        attrs = dict(self.attrs, **kwargs)
        attrs['class'] = base_css
        if 'data_tip' in attrs:
            del(attrs['data_tip'])
        if 'title_tip' in attrs:
            attrs['data-tip'] = attrs['title_tip']
            del(attrs['title_tip'])
        if extra_css:
            for css in extra_css:
                attrs['class'] += u' %s-%s' % (base_css, slugify(u'%s' % css))
        return attrs

    

class TextWidget(Widget):
    css = 'widget-text'

    def _format_value(self, value, item):
        if self.field.choices:
            return force_text(dict(self.field.flatchoices).get(value, value), strings_only=True)
        if self.is_localized:
            return formats.localize_input(value)
        return value
    
    def render_title(self, writer, name, attrs=None):
        if self.field.title is None:
            value = u''
        else:
            value = self.field.title
        if not isinstance(writer, HtmlWriter):
            return value
        final_attrs = self.build_title_attrs(base_css=self.css, extra_css=(name.replace('_', '-'),))
        return format_html(u'<span{0} >{1}</span>', flatatt(final_attrs), force_text(value))
        
    def render(self, writer, name, value, data, attrs=None):
        if value is None:
            value = u''
        if not isinstance(writer, HtmlWriter):
            return value
        final_attrs = self.build_attrs(data=data, base_css=self.css, extra_css=(name.replace('_', '-'),))
        if callable(value):
            value = value()
        return format_html(u'<span{0} data-raw="{1}">{2}</span>', flatatt(final_attrs), force_text(value), force_text(self._format_value(value, data)))

class UrlWidget(TextWidget):
    """
    Renders the value inside an <a> tag.
    """
    css = 'widget-url'

class ForeignKeyWidget(UrlWidget):
    """
    Renders the value inside an <a> tag.
    
    @todo: Write rendering function.
    """
    css = 'widget-foreign-key'
    
    def render(self, writer, name, value, data, attrs=None, is_self=False):
        final_attrs = self.build_attrs(data=data, base_css=self.css, extra_css=(name.replace('_', '-'),))
        
        if is_self:
            obj = data
        else:
            obj = value
        
        if not isinstance(obj, models.Model) and obj != None:
            raise Exception("ForeignKeyWidget expects value to be an instance of Model not %s" % (obj))
        
        if not isinstance(writer, HtmlWriter):
            return obj
        
        if 'get_absolute_url' in dir(obj):
            return format_html(
                u'<span{0} ><a href="{1}">{2}</a></span>', 
                flatatt(final_attrs), 
                obj.get_absolute_url(),
                value
            )
        else:
            return format_html(
                u'<span{0} >{1}</span>', 
                flatatt(final_attrs), 
                value
            )

class EmailWidget(UrlWidget):
    css = 'widget-email'
    
    def render(self, writer, name, value, data, attrs=None, is_self=False):
        final_attrs = self.build_attrs(data=data, base_css=self.css, extra_css=(name.replace('_', '-'),))

        if not isinstance(writer, HtmlWriter):
            return value
        
        return format_html(
            u'<span{0} ><a href="mailto:{1}">{2}</a></span>', 
            flatatt(final_attrs), 
            value,
            value
        )

class SelfWidget(ForeignKeyWidget):
    css = 'widget-self'
    
    def render(self, writer, name, value, data, attrs=None):
        return super(SelfWidget, self).render(writer, name, value, data, attrs=None, is_self=True)

class CommaSeparatedWidget(TextWidget):
    css = 'widget-comma-separated'

    def render(self, writer, name, value, data, attrs=None):
        final_attrs = self.build_attrs(data=data, base_css=self.css, extra_css=(name.replace('_', '-'),))
        items = []

        if callable(value):
            value = value()
        
        for item in value:
            items.append(self.render_item(item, final_attrs, writer, name, value, data, attrs))
        return ", ".join(items)

class CommaSeparatedUrlWidget(CommaSeparatedWidget):
    css = 'widget-comma-separated-url'

    def render_item(self, item, final_attrs, writer, name, value, data, attrs=None):
        if not isinstance(writer, HtmlWriter):
            return "%s" % item
        else:
            return format_html(
                u'<span{0} ><a href="{1}">{1}</a></span>', 
                flatatt(final_attrs), 
                item
            )
            
class CommaSeparatedEmailWidget(CommaSeparatedWidget):
    css = 'widget-comma-separated-email'

    def render_item(self, item, final_attrs, writer, name, value, data, attrs=None):
        if not isinstance(writer, HtmlWriter):
            return "%s" % item
        else:
            return format_html(
                u'<span{0} ><a href="mailto:{1}">{1}</a></span>', 
                flatatt(final_attrs), 
                item
            )

class CommaSeparatedForeignKeyWidget(CommaSeparatedUrlWidget):
    css = 'widget-comma-separated-foreign-key'
    
    def render_item(self, item, final_attrs, writer, name, value, data, attrs=None):
        if not isinstance(item, models.Model) and item != None:
            raise Exception("CommaSeparatedForeignKeyWidget expects value to be an instance of Model not %s" % (item))
        
        if not isinstance(writer, HtmlWriter):
            return "%s" % item
        else:
            if 'get_absolute_url' in dir(item):
                return format_html(
                    u'<span{0} ><a href="{1}">{2}</a></span>', 
                    flatatt(final_attrs), 
                    item.get_absolute_url(),
                    item
                )
            else:
                return format_html(
                    u'<span{0} >{1}</span>', 
                    flatatt(final_attrs), 
                    item
                )

class DecimalWidget(TextWidget):
    css = 'widget-decimal'
    
    def _format_value(self, value, item):
        if self.field.only_necessary_places:
            return "%s" % round(value, self.field.decimal_places) if value % 1 else int(value)
        else:
            return "%s" % Decimal(value).quantize(Decimal('.%s' % '0'.zfill(self.field.decimal_places)))  

class PercentWidget(DecimalWidget):
    css = 'widget-percent'
    
    def render(self, writer, name, value, data, attrs=None):
        if value is None:
            value = (Decimal('0'), Decimal('0'))
        else:
            if value[1] == 0:
                percent = Decimal('0')
            else:
                percent = (Decimal(value[0]) / Decimal(value[1])) * 100
        quantize = Decimal('.' + '0'.zfill(self.field.decimal_places))
        if not isinstance(writer, HtmlWriter):
            return ('%s%% (%s/%s)') % (
                self._format_value(percent, data), 
                self._format_value(value[0], data), 
                self._format_value(value[1], data)
            )
        final_attrs = self.build_attrs(data=data, base_css=self.css, extra_css=(name.replace('_', '-'),))
        return format_html(
            u'<span{0} ><span class="{1}-percent">{2}%</span> <span class="{3}-values">({4}/{5})</span></span>', 
            flatatt(final_attrs), 
            self.css, 
            self._format_value(percent, data), 
            self.css, 
            self._format_value(value[0], data), 
            self._format_value(value[1], data)
        )

class CountWidget(DecimalWidget):
    css = 'widget-count'
    
    def render(self, writer, name, value, data, attrs=None):
        if value is None:
            count = 0
        elif hasattr(value, 'count') and callable(value.count):
            count = value.count()
        else:
            count = len(value)
        if not isinstance(writer, HtmlWriter):
            return count
        final_attrs = self.build_attrs(data=data, base_css=self.css, extra_css=(name.replace('_', '-'),))
        return format_html(
            u'<span{0} >{1}</span>', 
            flatatt(final_attrs), 
            count
        )

class DateWidget(TextWidget):
    css = 'widget-date'

    def __init__(self, field, attrs=None, format=None):
        super(DateWidget, self).__init__(field, attrs)
        if format:
            self.format = format
            self.manual_format = True
        else:
            self.format = formats.get_format('DATE_INPUT_FORMATS')[0]
            self.manual_format = False

    def _format_value(self, value, item):
        from django_datatables.fields import F

        if self.field.timesince and value:
            try:
                now = None
                if isinstance(self.field.timesince, F):
                    now = self.field.traverse_for_value(item, self.field.timesince.name)
                return timesince(value, now=now, reversed=self.field.timesince_reverse)
            except AttributeError: pass # perhaps its not a date/datetime...
        if self.is_localized and not self.manual_format:
            return formats.localize_input(value)
        elif hasattr(value, 'strftime'):
            value = datetime_safe.new_date(value)
            return value.strftime(self.format)
        return value
    
class DateTimeWidget(TextWidget):
    css = 'widget-datetime'
    
    def __init__(self, field, attrs=None, format=None):
        super(DateTimeWidget, self).__init__(field, attrs)
        if format:
            self.format = format
            self.manual_format = True
        else:
            self.format = formats.get_format('DATETIME_INPUT_FORMATS')[0]
            self.manual_format = False

    def _format_value(self, value, item):
        if self.is_localized and not self.manual_format:
            return formats.localize_input(value)
        elif hasattr(value, 'strftime'):
            value = datetime_safe.new_datetime(value)
            return value.strftime(self.format)
        return value

class ActionWidgetItem():
    pass

class ActionWidgetImageButton(ActionWidgetItem):
    """Markup follows twitter bootstrap style"""
    
    def __init__(self, title, btn_class=False, icon_class=False, href=False):
        self.icon_class = icon_class
        self.title = title
        self.href = href
        self.btn_class = btn_class
    
    def render(self):
        title = '<i class="%s"></i> %s' % (self.icon_class, self.title) if self.icon_class else self.title
        if self.href:
            return '<a href="%s" class="btn %s">%s</a>' % (self.href, self.btn_class, title)
        else:
            return '<button class="btn %s">%s</button>' % (self.btn_class, title)

class ActionsWidget(UrlWidget):
    css = 'widget-actions'
    
    def render(self, writer, name, value, data, attrs=None):
        """
        Render a list of ActionWidgetItem's
        """
        if not isinstance(writer, HtmlWriter):
            # This type of widget only supports html
            return None
        content = ''
        if value:
            for item in value(request=self.field.datatable.request):
                content += item.render()
            final_attrs = self.build_attrs(item=item, base_css=self.css, extra_css=(name.replace('_', '-'),))
            return format_html(
                u'<span{0}>{1}</span>', 
                flatatt(final_attrs), 
                mark_safe(content)
            )

class SpanWidget(TextWidget):
    
    def render(self, writer, name, value, data, attrs=None):
        if value is None:
            value = u''
        if not isinstance(writer, HtmlWriter):
            return value
        final_attrs = self.build_attrs(item=data, base_css=self.css, extra_css=(name.replace('_', '-'),))
        if callable(value):
            value = value()
        
        # Now work out the label class
        extra_css_classes = self.get_extra_css_classes()
        if hasattr(data, extra_css_classes):
            extra_css_classes = getattr(data, extra_css_classes)
            if callable(extra_css_classes):
                extra_css_classes = extra_css_classes()
        return format_html(u'<span{0} data-raw="{1}"><span class="{2} {3}">{4}</span></span>', 
                           flatatt(final_attrs), 
                           force_text(value), 
                           self.base_css_class,
                           extra_css_classes, 
                           mark_safe(force_text(self._format_value(value, data))))

class LabelWidget(SpanWidget):
    css = 'widget-label'
    base_css_class = 'label'
    
    def get_extra_css_classes(self):
        return self.field.label_class
    
class BadgeWidget(SpanWidget):
    css = 'widget-badge'
    base_css_class = 'badge'
    
    def get_extra_css_classes(self):
        return self.field.badge_class
    