from django.utils.text import slugify
from django_datatables.writers.html import HtmlWriter

def context_helper(datatable, source_url, create_url=False):
    context = {
        'id':           slugify(u'%s-%s' % (datatable.__module__.replace('.', '-'), datatable.__class__.__name__)),
        'source_url':   source_url,
        'verbose_name': datatable._meta.verbose_name,
        'description':  datatable._meta.description,
        'datatable':    datatable,
        'writer':       HtmlWriter(datatable)
    }
    if create_url:
        context['create_url'] = create_url
    return context