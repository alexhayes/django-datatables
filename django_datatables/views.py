import os
import cStringIO
import logging
from django.http.response import HttpResponse 
from datetime import datetime
from django.core.servers.basehttp import FileWrapper
from django.views.generic.base import TemplateView
from django.utils.text import slugify
from django_datatables.helper import context_helper
from django_datatables.writers.csv_writers import UnicodeCsvWriter
from django_datatables.writers.json import JsonWriter
from django_datatables_view.mixins import JSONResponseView

logger = logging.getLogger(__name__)

class DatatableView(JSONResponseView):
    template_name = "datatables/wrapper.html"
    datatable = None
    
    def initialize(self, *args, **kwargs):
        self.datatable = self.datatable()
        self.datatable.request = self.request
    
    def is_json(self):
        return self.request.REQUEST.get('json', False) != False
    
    def is_csv(self):
        return self.request.REQUEST.get('csv', False) != False
    
    def get(self, request, *args, **kwargs):
        """
        Return either a html, json or csv response.
        """
        self.initialize()
        if self.is_json():
            return JSONResponseView.get(self, request, *args, **kwargs)
        elif self.is_csv():
            return self.csv(request, *args, **kwargs)
        else:
            return TemplateView.get(self, request, *args, **kwargs)

    def csv_filename(self):
        return '%s-%s.csv' % (slugify(u'%s' % self.datatable.__module__.replace('.', '-')), datetime.now().strftime('%Y%m%d%H%M%S'))

    def csv(self, request, *args, **kwargs):
        """
        Send csv download to client.
        """
        # Create csv
        csv_handle = cStringIO.StringIO()
        writer = UnicodeCsvWriter(self.datatable, self.datatable.queryset())
        writer.write(csv_handle)
        csv_handle.reset()
        
        # Get size of csv
        csv_handle.seek(0, os.SEEK_END)
        size = csv_handle.tell()
        csv_handle.reset()
        
        wrapper = FileWrapper(csv_handle)
        response = HttpResponse(wrapper, mimetype='text/csv')
        response['Accept-Ranges'] = 'bytes'
        response['Content-Length'] = size
        response['Content-Disposition'] = 'attachment; filename="%s"' % self.csv_filename()
        return response
    
    def render_to_response(self, context):
        """
        Render either a html or json response.
        """
        if self.is_json():
            return JSONResponseView.render_to_response(self, context)
        else:
            return TemplateView.render_to_response(self, context)

    def get_json_context_data(self, *args, **kwargs):
        qs = self.get_initial_queryset(*args, **kwargs)

        # number of records before filtering
        total_records = qs.count()

        qs = self.filter_queryset(qs)

        # number of records after filtering
        total_display_records = qs.count()

        qs = self.ordering(qs)
        qs = self.paging(qs)

        # prepare output data
        aaData = self.prepare_results(qs)

        context = {
            'sEcho': int(self.request.REQUEST.get('sEcho', 0)),
            'iTotalRecords': total_records,
            'iTotalDisplayRecords': total_display_records,
            'aaData': aaData
        }
        return context

    def get_html_context_data(self, *args, **kwargs):
        # Return context to build the html for the datatable.
        context = super(TemplateView, self).get_context_data(**kwargs)
        context['datatable'] = context_helper(self.datatable, self.request.path)
        return context

    def get_context_data(self, *args, **kwargs):
        if self.is_json():
            return self.get_json_context_data()
        else:
            return self.get_html_context_data()

    def get_order_columns(self):
        """ Return list of columns used for ordering
        """
        return [field.name for field in self.datatable.fields()]

    def get_initial_queryset(self, *args, **kwargs):
        """
        Get initial query set.
        
        Return queryset used as base for further sorting/filtering
        
        @see: BaseDatatableView
        """
        return self.datatable.queryset()

    def filter_queryset(self, qs):
        """
        Filter the queryset using parameters available in the request.
        
        @see: BaseDatatableView
        """
        # First process the all fields search
        sSearch = self.request.REQUEST.get('sSearch', None)
        
        qs_params = self.datatable.get_qs_for_term(sSearch)

        if qs_params:
            qs = qs.filter(qs_params)

        # Now process the column search for each column
        sColumns = self.request.REQUEST.get('sColumns', None)
        if sColumns:
            sColumns = sColumns.split(',')
            for i, field_name in enumerate(sColumns):
                term = self.request.REQUEST.get('sSearch_%s' % i, None)
                if term:
                    qs_params = self.datatable.field(field_name).get_qs_for_term(term, True)
                    if qs_params:
                        qs = qs.filter(qs_params)
        
        return qs

    def prepare_results(self, qs):
        """
        Prepare list with output column data - queryset is already paginated here.
        
        @see: BaseDatatableView
        """
        return JsonWriter(self.datatable).as_json(qs)

    def ordering(self, qs):
        """ Get parameters from the request and prepare order by clause
        """
        # Number of columns that are used in sorting
        try:
            i_sorting_cols = int(self.request.REQUEST.get('iSortingCols', 0))
        except ValueError:
            i_sorting_cols = 0

        order = []
        order_columns = self.get_order_columns()
        for i in range(i_sorting_cols):
            # sorting column
            try:
                i_sort_col = int(self.request.REQUEST.get('iSortCol_%s' % i))
            except ValueError:
                i_sort_col = 0
            # sorting order
            s_sort_dir = self.request.REQUEST.get('sSortDir_%s' % i)

            sdir = '-' if s_sort_dir == 'desc' else ''

            sortcol = order_columns[i_sort_col]
            if isinstance(sortcol, list):
                for sc in sortcol:
                    order.append('%s%s' % (sdir, sc))
            else:
                order.append('%s%s' % (sdir, sortcol))
        if order:
            return qs.order_by(*order)
        return qs

    def paging(self, qs):
        """ Paging
        """
        limit = min(int(self.request.REQUEST.get('iDisplayLength', 10)), 100)
        # if pagination is disabled ("bPaginate": false)
        if limit == -1:
            return qs
        start = int(self.request.REQUEST.get('iDisplayStart', 0))
        offset = start + limit
        return qs[start:offset]

