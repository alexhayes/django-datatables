import csv, logging
from django_datatables.writers.base import BaseWriter
from django.utils.safestring import mark_safe
from django.utils import formats, simplejson
from django.utils.text import slugify
from django.utils.encoding import force_text
from django.forms.util import flatatt
from django.utils.html import format_html
from django_datatables_view.mixins import DTEncoder
from django.forms.widgets import Select
from collections import OrderedDict

class HtmlWriter(BaseWriter):
    """
    Helper class to aid in writing the HTML table in conjunction with table.html.
    """
    def _get_filter_type_for_field(self, field):
        from django_datatables import fields
        if isinstance(field, fields.CharField):
            if field.choices:
                return 'select'
            return 'text'
        if isinstance(field, fields.IntegerField):
            return 'number'
        if isinstance(field, fields.DateTimeField) or isinstance(field, fields.DateField):
            return 'date-range'
        return 'text'
    
    def thead(self):
        ths = []
        for field in self.datatable.fields():
            slug = slugify(u'%s' % field.name.replace('_', '-'))
            css = [field.widget.css, '%s-%s' % (field.widget.css, slug)]
            data_filter = ''
            if field.sorting:
                css.append('sorting')
            if field.filter:
                css.append('filtering')
                data_filter += ' data-filter-selector="%s" data-filter-type="%s"' % (field.name, self._get_filter_type_for_field(field))
                if field.choices:
                    choices = [{'value': choice[0], 'label': choice[1]} for choice in field.choices]
                    data_filter += " data-filter-choices='%s'" % (simplejson.dumps(choices, cls=DTEncoder))
            ths.append('<th class="%s"%s>%s</th>' % (" ".join(css), data_filter, field.widget.render_title(self, field.name)))
        return mark_safe("\n".join(ths))
    
    def has_filters(self):
        return len([field for field in self.datatable.fields() if field.filter]) > 0
    
    def filters(self):
        ths = []
        for field in self.datatable.fields():
            input = ''
            css = [field.widget.css, '%s-%s' % (field.widget.css, slugify(u'%s' % field.name.replace('_', '-')))]
            ths.append('<th class="%s">%s</th>' % (' '.join(css), input))
        return mark_safe("\n".join(ths))
    
    def as_table(self):
        rows = []
        for item in self.datatable.data():
            row = []
            for field in self.datatable.fields():
                row.append("<td>%s</td>" % field.widget.render(self, field.name, field.traverse_for_value(item), item))
            rows.append("<tr>%s</tr>\n" % "\n".join(row))
            
        return mark_safe('\n'.join(rows))
