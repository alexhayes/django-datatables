from django_datatables.writers.html import HtmlWriter

class JsonWriter(HtmlWriter):
    """
    Json report writer.
    """
    report = None

    def as_json(self, qs):
        rows = []
        for row in qs:
            _row = {}
            
            if 'get_row_id' in dir(row):
                _row['DT_RowClass'] = row.get_row_class()
            if 'get_row_class' in dir(row):
                _row['DT_RowClass'] = row.get_row_class()
            
            i = 0
            for field in self.datatable.fields():
                _row[i] = field.widget.render(self, field.name, field.traverse_for_value(row), row)
                i += 1
            rows.append(_row)
        return rows