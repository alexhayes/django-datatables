# Django Datatables

Define your datatables in Python using an API based on Django's model API.

# Requirements

## Python

django-datatables-view

## CSS

jQuery UI
jQuery Ui Bootstrap
Datatables

## JS

jQuery UI
jQuery Ui Bootstrap
Datatables

	<script type="text/javascript" src="/static/js/jquery-1.9.1.js" charset="utf-8"></script>
	<script type="text/javascript" src="/static/js/jquery-ui-1.10.2.custom.js" charset="utf-8"></script>
	<script type="text/javascript" src="/static/js/bootstrap/bootstrap-affix.js" charset="utf-8"></script>
	<script type="text/javascript" src="/static/js/bootstrap/bootstrap-alert.js" charset="utf-8"></script>
	<script type="text/javascript" src="/static/js/bootstrap/bootstrap-button.js" charset="utf-8"></script>
	<script type="text/javascript" src="/static/js/bootstrap/bootstrap-carousel.js" charset="utf-8"></script>
	<script type="text/javascript" src="/static/js/bootstrap/bootstrap-collapse.js" charset="utf-8"></script>
	<script type="text/javascript" src="/static/js/bootstrap/bootstrap-dropdown.js" charset="utf-8"></script>
	<script type="text/javascript" src="/static/js/bootstrap/bootstrap-modal.js" charset="utf-8"></script>
	<script type="text/javascript" src="/static/js/bootstrap/bootstrap-scrollspy.js" charset="utf-8"></script>
	<script type="text/javascript" src="/static/js/bootstrap/bootstrap-tab.js" charset="utf-8"></script>
	<script type="text/javascript" src="/static/js/bootstrap/bootstrap-tooltip.js" charset="utf-8"></script>
	<script type="text/javascript" src="/static/js/bootstrap/bootstrap-popover.js" charset="utf-8"></script>
	<script type="text/javascript" src="/static/js/bootstrap/bootstrap-transition.js" charset="utf-8"></script>
	<script type="text/javascript" src="/static/js/bootstrap/bootstrap-typeahead.js" charset="utf-8"></script>
	<script type="text/javascript" src="/static/js/bootstrap-notify.js" charset="utf-8"></script>
	<script type="text/javascript" src="/static/js/datatables/js/jquery.dataTables.js" charset="utf-8"></script>
	<script type="text/javascript" src="/static/js/datatables-extras/TableTools/media/js/ZeroClipboard.js" charset="utf-8"></script>
	<script type="text/javascript" src="/static/js/datatables-extras/TableTools/media/js/TableTools.js" charset="utf-8"></script>
	<script type="text/javascript" src="/static/js/jquery.dataTables.ColumnFilter.js" charset="utf-8"></script>
	<script type="text/javascript" src="/static/js/jquery.dataTables.Bootstrap.js" charset="utf-8"></script>
	<script type="text/javascript" src="/static/js/datatables.js" charset="utf-8"></script>


# Usage



# Filter querysets with request parameters

To filter the queryset using request parameters you need to define a
`get_initial_queryset` method inside a custom view which extends 
DatatableView.